-- +===========================================================================+
-- | Name       : install.sql                                                  |
-- | Project    : Oracle plsql coding challenge.                               |
-- | Description:                                                              |
-- |                                                                           |
-- | Related Doc:                                                              |
-- |                                                                           |
-- |Version  Date         Author             Comments                          |
-- |=======  ===========  =================  ================================= |
-- |1.0      24-MAY-2021  Bharat Bhagnani    Initial Version                   |
-- +===========================================================================+

SET SHOW         OFF
SET VERIFY       OFF
SET ECHO         OFF
SET TAB          OFF
SET FEEDBACK     OFF

WHENEVER SQLERROR EXIT 1 ROLLBACK
PROMPT
PROMPT Droping TABLE dept
PROMPT

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE dept';
EXCEPTION
WHEN OTHERS THEN 
  IF SQLCODE != -942 THEN
         RAISE;
  END IF;
END ;
/

PROMPT
PROMPT Droping TABLE emp
PROMPT
BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE emp';
EXCEPTION
WHEN OTHERS THEN 
  IF SQLCODE != -942 THEN
         RAISE;
  END IF;
END ;
/


REM ************************************************
REM Creating/Populating TABLE dept
REM ************************************************

PROMPT
PROMPT Creating TABLE dept
PROMPT
CREATE TABLE dept (
         department_id      NUMBER(5) GENERATED ALWAYS AS IDENTITY START WITH 1 INCREMENT BY 1 NOCACHE,
         dname              VARCHAR2(50) NOT NULL,
         location           VARCHAR2(50) NOT NULL
   TABLESPACE xx_tbs;
   
PROMPT
PROMPT Populating TABLE dept
PROMPT
INSERT INTO dept (dname, location) VALUES ('Management', 'London');
INSERT INTO dept (dname, location) VALUES ('Engineering', 'Cardiff');
INSERT INTO dept (dname, location) VALUES ('Research & Development', 'Edinburgh');
INSERT INTO dept (dname, location) VALUES ('Sales', 'Belfast');

PROMPT
PROMPT Creating TABLE emp
PROMPT
CREATE TABLE emp (
         employee_id        NUMBER(10) GENERATED ALWAYS AS IDENTITY START WITH 90000 INCREMENT BY 1 NOCACHE,
         ename              VARCHAR2(50) NOT NULL,
         job_title          VARCHAR2(50) NOT NULL,
         mgr_id             NUMBER(10),         
         hiredate           DATE NOT NULL,
         salary             NUMBER(10) NOT NULL,
         department_id      NUMBER(5) NOT NULL
                     CONSTRAINT dept_fkey REFERENCES dept
                     (department_id))
   TABLESPACE xx_tbs;

PROMPT
PROMPT Populating TABLE emp
PROMPT
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('John Smith', 'CEO', null, to_date('01/01/1995','DD/MM/YYYY'), 100000, (select department_id from dept where dname = 'Management' and rownum <= 1) );
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Jimmy Willis', 'Manager', null, to_date('23/09/2003','DD/MM/YYYY'), 52500, (select department_id from dept where dname = 'Sales' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Roxy Jones', 'Salesperson', null, to_date('11/02/2017','DD/MM/YYYY'), 35000, (select department_id from dept where dname = 'Sales' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Selwyn Field', 'Salesperson', null, to_date('20/05/2015','DD/MM/YYYY'), 32000, (select department_id from dept where dname = 'Sales' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('David Hallett', 'Engineer', null, to_date('17/04/2018','DD/MM/YYYY'), 40000, (select department_id from dept where dname = 'Engineering' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Sarah Phelps', 'Manager', null, to_date('21/03/2015','DD/MM/YYYY'), 45000, (select department_id from dept where dname = 'Engineering' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Louise Harper', 'Engineer', null, to_date('01/01/2013','DD/MM/YYYY'), 47000, (select department_id from dept where dname = 'Engineering' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Tina Hart', 'Engineer', null, to_date('28/07/2014','DD/MM/YYYY'), 45000, (select department_id from dept where dname = 'Research & Development' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Gus Jones', 'Manager', null, to_date('15/05/2018','DD/MM/YYYY'), 50000, (select department_id from dept where dname = 'Research & Development' and rownum <= 1));
INSERT INTO emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
            VALUES ('Mildred Hall', 'Secretary', null, to_date('12/10/1996','DD/MM/YYYY'), 35000, (select department_id from dept where dname = 'Management' and rownum <= 1));

PROMPT
PROMPT Updating manager details for the employees.
PROMPT
UPDATE emp
SET mgr_id = (SELECT employee_id FROM emp WHERE ename = 'John Smith')
WHERE ename IN ('Jimmy Willis', 'Sarah Phelps', 'Gus Jones', 'Mildred Hall');

UPDATE emp
SET mgr_id = (SELECT employee_id FROM emp WHERE ename = 'Sarah Phelps')
WHERE ename IN ('David Hallett', 'Louise Harper');

UPDATE emp
SET mgr_id = (SELECT employee_id FROM emp WHERE ename = 'Gus Jones')
WHERE ename IN ('Tina Hart');


SET SERVEROUTPUT ON;
CREATE OR REPLACE PACKAGE employee AS

gcc_pkg_name     CONSTANT    VARCHAR2(30) := 'employee';

FUNCTION get_emp_id 
( p_ename                IN     VARCHAR2 DEFAULT NULL
) RETURN NUMBER;

FUNCTION get_dept_id
( p_dname                IN     VARCHAR2 DEFAULT NULL
) RETURN NUMBER;

PROCEDURE create 
( p_ename                IN     VARCHAR2  
, p_job_title            IN     VARCHAR2
, p_mgr_id               IN     NUMBER DEFAULT 0
, p_mgr_name             IN     VARCHAR2 DEFAULT NULL
, p_hiredate             IN     DATE
, p_salary               IN     NUMBER
, p_department_id        IN     NUMBER   DEFAULT 0
, p_dname                IN     VARCHAR2 DEFAULT NULL
);

PROCEDURE maintain_salary 
( p_employee_id          IN     NUMBER   DEFAULT 0
, p_ename                IN     VARCHAR2 DEFAULT NULL
, p_operator             IN     VARCHAR2 DEFAULT NULL
, p_percentage           IN     NUMBER   DEFAULT 0
);

PROCEDURE transfer 
( p_employee_id          IN     NUMBER
, p_from_dept_id         IN     NUMBER
, p_to_dept_id           IN     NUMBER 
);

PROCEDURE transfer 
( p_employee_id          IN     NUMBER   DEFAULT 0
, p_ename                IN     VARCHAR2 DEFAULT NULL
, p_from_dname           IN     VARCHAR2
, p_to_dname             IN     VARCHAR2 
);

FUNCTION get_salary 
( p_employee_id          IN     NUMBER   DEFAULT 0
, p_ename                IN     VARCHAR2 DEFAULT NULL
) RETURN NUMBER;

PROCEDURE dept_report 
( p_department_id        IN     NUMBER   DEFAULT 0
, p_dname                IN     VARCHAR2 DEFAULT NULL
, p_summ_det             IN     VARCHAR2 DEFAULT 'DETAIL'
);

END employee;
/
SHOW ERRORS
/


CREATE OR REPLACE PACKAGE BODY employee AS

-- Assumptions:
-- Any exceptions, are shown on the SQL console assuming that the package call is from SQLPlus and SET SERVEROUTPUT is ON

--+----------------------------------------------------------------------------+
--| Subprogram : get_emp_id                                                    |
--| Scope      : Package function, can be called externally ie. SQLPlus        |
--| Description: Takes employee name and returns employee id after comparing   |
--|              employee name in upper case from employee table.              |
--|                                                                            |
--| Usage      : lvn_employee_id := employee.get_emp_id('John Smith');         |
--+----------------------------------------------------------------------------+
FUNCTION get_emp_id 
( p_ename                IN     VARCHAR2 DEFAULT NULL
) RETURN NUMBER
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'get_emp_id';
lvn_employee_id                   NUMBER          := 0;
BEGIN
    SELECT emp.employee_id
    INTO lvn_employee_id
    FROM emp 
    WHERE upper(emp.ename) = upper(p_ename)
    AND rownum <= 1;
    RETURN lvn_employee_id;    
EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE()); 
    RETURN lvn_employee_id;
END get_emp_id;

--+----------------------------------------------------------------------------+
--| Subprogram : get_dept_id                                                   |
--| Scope      : Package function, can be called externally ie. SQLPlus        |
--| Description: Takes department name and returns department id after         |
--|              comparing department name in upper case from department table.|
--|                                                                            |
--| Usage      :                                                               |
--|  lvn_department_id := employee.get_dept_id('Management');                  |
--+----------------------------------------------------------------------------+
FUNCTION get_dept_id
( p_dname                IN     VARCHAR2 DEFAULT NULL
) RETURN NUMBER
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'get_dept_id';
lvn_department_id                 NUMBER          := 0;
BEGIN
    SELECT dept.department_id
    INTO lvn_department_id
    FROM dept 
    WHERE upper(dept.dname) = upper(p_dname)
    AND rownum <= 1;
    RETURN lvn_department_id;    
EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE()); 
    RETURN lvn_department_id;
END get_dept_id;

--+----------------------------------------------------------------------------+
--| Subprogram : create                                                        |
--| Scope      : Package procedure, can be called externally ie. SQLPlus       |
--| Description: Takes employee details and checks for duplicate employee name,|
--|              validates mgr already exists before inserting the new emp rec |
--|              in the employee table.                                        |
--|                                                                            |
--| Usage      :                                                               |
--|  employee.create(p_ename => 'Employee Name'                                |
--|                 ,p_job_title => 'Title'                                    |
--|                 ,p_mgr_id => null                                          |
--|                 ,p_mgr_name => 'Manager Name'                              |
--|                 ,p_hiredate => '01/01/2021'                                |
--|                 ,p_salary => 100000                                        |
--|                 ,p_department_id => null                                   |
--|                 ,p_dname => 'Department Name');                            |
--+----------------------------------------------------------------------------+
PROCEDURE create 
( p_ename                IN     VARCHAR2  
, p_job_title            IN     VARCHAR2
, p_mgr_id               IN     NUMBER DEFAULT 0
, p_mgr_name             IN     VARCHAR2 DEFAULT NULL
, p_hiredate             IN     DATE
, p_salary               IN     NUMBER
, p_department_id        IN     NUMBER   DEFAULT 0
, p_dname                IN     VARCHAR2 DEFAULT NULL
)
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'create';
lvn_employee_id                   emp.employee_id%type := 0;
lvn_count                         NUMBER := 0;
lvc_exist                         VARCHAR2(1) := 'N';
lvn_mgr_id                        NUMBER := 0;
BEGIN
    -- validating if employee already exists?
    IF get_emp_id(p_ename => p_ename) > 0 THEN 
        dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || 'Employee record already exists for this employee name, no records created.');
        RETURN;
    END IF;
    
    -- validating if mgr details exists or not. Id takes precedence on name.
    IF p_mgr_id > 0 THEN 
        SELECT 'Y' INTO lvc_exist FROM emp WHERE emp.employee_id = p_mgr_id;
    ELSIF p_mgr_name is not null THEN 
        lvn_mgr_id := get_emp_id(p_ename => p_mgr_name) ;
        lvc_exist := 'Y';
    END IF;
    
    INSERT INTO  emp (ename, job_title, mgr_id, hiredate, salary, department_id) 
         VALUES (p_ename, p_job_title, case when p_mgr_id > 0 and lvc_exist = 'Y' then p_mgr_id 
                                            when lvn_mgr_id > 0 and lvc_exist = 'Y' then lvn_mgr_id
                                       else null end 
                , to_date(p_hiredate,'DD/MM/YYYY'), p_salary
                -- department id takes precedence over dept name.
                , case when p_department_id > 0 then p_department_id else get_dept_id( p_dname => p_dname ) end )
         RETURNING emp.employee_id INTO lvn_employee_id;

    lvn_count := sql%rowcount;
    IF  lvn_count = 0 THEN
        dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || 'No employee record created, incorrect id/value provided the department.');
    ELSE
        dbms_output.put_line(to_char(lvn_count) || ' record created with employee_id : ' || to_char(lvn_employee_id));
    END IF;
EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE());  
END create;


--+----------------------------------------------------------------------------+
--| Subprogram : maintain_salary                                               |
--| Scope      : Package procedure, can be called externally ie. SQLPlus       |
--| Description: Takes employee details and other parameters to make changes to|
--|              employee salary. Salary increments or decrements based on the |
--|              parameters provided.                                          |
--|                                                                            |
--| Usage      :                                                               |
--|  employee.maintain_salary(p_employee_id => null                            |
--|                          ,p_ename => 'Employee Name'                       |
--|                          ,p_operator => '+'                                |
--|                          ,p_percentage => 10);                             |
--+----------------------------------------------------------------------------+
PROCEDURE maintain_salary 
( p_employee_id          IN     NUMBER   DEFAULT 0
, p_ename                IN     VARCHAR2 DEFAULT NULL
, p_operator             IN     VARCHAR2 DEFAULT NULL
, p_percentage           IN     NUMBER   DEFAULT 0
)
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'maintain_salary';
lvn_salary                        NUMBER := -1;
lvn_count                         NUMBER := 0;

BEGIN
    IF p_operator IN ('+','-') THEN         
        UPDATE emp 
        SET emp.salary = case when p_operator = '+' 
                                then emp.salary + ( emp.salary * p_percentage / 100)
                            when p_operator = '-' 
                                then emp.salary - ( emp.salary * p_percentage / 100)
                            else emp.salary
                        end
        -- employee id takes precendence over emp name.
        WHERE emp.employee_id = case when p_employee_id > 0 then p_employee_id else get_emp_id (p_ename => p_ename) end
        RETURNING emp.salary INTO lvn_salary;
        lvn_count := sql%rowcount;
        IF lvn_count = 0 THEN
            dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || 'No employee record updated, employee id/value did not match.');  
        ELSIF lvn_count > 0 and lvn_salary <= 0 THEN 
            dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || 'Salary can not be negative or zero, hence rolling back changes.');
            ROLLBACK;
        END IF;
    ELSE
        dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || 'Operator needs to be either + to increment the salary or - to decrement the salary.');
    END IF;
EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE());  
END maintain_salary;

--+----------------------------------------------------------------------------+
--| Subprogram : transfer                                                      |
--| Scope      : Package procedure, can be called externally ie. SQLPlus       |
--| Description: Takes employee and department details to perform employee trf.|
--|                                                                            |
--| Usage      :                                                               |
--|  employee.transfer(p_employee_id => 99999                                  |
--|                   ,p_from_dept_id => 9                                     |
--|                   ,p_to_dept_id => 9);                                     |
--+----------------------------------------------------------------------------+
PROCEDURE transfer 
( p_employee_id          IN     NUMBER
, p_from_dept_id         IN     NUMBER
, p_to_dept_id           IN     NUMBER 
)
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'transfer';
lvn_count                         NUMBER := 0;
BEGIN
    UPDATE emp 
    SET emp.department_id = p_to_dept_id
    WHERE emp.employee_id = p_employee_id
      AND emp.department_id = p_from_dept_id ;
    
    lvn_count := sql%rowcount;
    IF lvn_count = 0 THEN
        dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || 'No employee record updated, employee or department id did not match.');
    ELSE
        dbms_output.put_line(to_char(lvn_count) || ' employee record updated for department transfer.');
    END IF;        

EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE());  
END transfer;

--+----------------------------------------------------------------------------+
--| Subprogram : transfer                                            OVERLOADED|
--| Scope      : Package procedure, can be called externally ie. SQLPlus       |
--| Description: Takes employee and department details to perform employee trf.|
--|                                                                            |
--| Usage      :                                                               |
--|  employee.transfer(p_employee_id => null`                                  |
--|                   ,p_ename => 'Employee Name'                              |
--|                   ,p_from_dname => 'From Department'                       |
--|                   ,p_to_dname => 'To Department');                         |
--+----------------------------------------------------------------------------+
PROCEDURE transfer 
( p_employee_id          IN     NUMBER   DEFAULT 0
, p_ename                IN     VARCHAR2 DEFAULT NULL
, p_from_dname           IN     VARCHAR2
, p_to_dname             IN     VARCHAR2 
)
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'transfer';
BEGIN

    -- additional validations can be added here to verify if department from and to are valid before calling below procedure.

    -- employee id takes precendence over emp name.
    transfer (p_employee_id  => case when p_employee_id > 0 then p_employee_id else get_emp_id (p_ename => p_ename) end
                 ,p_from_dept_id => get_dept_id (p_dname => p_from_dname)
                 ,p_to_dept_id   => get_dept_id (p_dname => p_to_dname)
                 );
EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE());  
END transfer;

--+----------------------------------------------------------------------------+
--| Subprogram : get_salary                                                    |
--| Scope      : Package function, can be called externally ie. SQLPlus        |
--| Description: Takes employee details and returns salary amount.             |
--|                                                                            |
--| Usage      :                                                               |
--|  lvn_salary := employee.get_salary( p_employee_id => 9999 );               |
--|  lvn_salary := employee.get_salary( p_ename => 'Employee Name');           |
--+----------------------------------------------------------------------------+
FUNCTION get_salary 
( p_employee_id          IN     NUMBER   DEFAULT 0
, p_ename                IN     VARCHAR2 DEFAULT NULL
) RETURN NUMBER
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'get_salary';
lvn_salary                        NUMBER          := 0;
BEGIN
    -- employee id takes precendence over emp name.
    SELECT emp.salary 
    INTO lvn_salary
    FROM emp 
    WHERE emp.employee_id =  case when p_employee_id > 0 then p_employee_id else get_emp_id (p_ename => p_ename) end;

    RETURN lvn_salary;    
EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE()); 
    RETURN lvn_salary;
END get_salary;

--+----------------------------------------------------------------------------+
--| Subprogram : dept_report                                                   |
--| Scope      : Package procedure, can be called externally ie. SQLPlus       |
--| Description: Takes department details and displays report ie: details of   |
--|              all the employees within the department or summary showing the|
--|              total salary in that department.                              |
--|                                                                            |
--| Usage      :                                                               |
--|  SET SERVEROUTPUT ON ;                                                     |
--|  execute employee.dept_report( p_dname => 'Deparment Name'                 |
--|                               ,p_summ_det => 'DETAIL');                    |
--|  execute employee.dept_report( p_dname => 'Deparment Name'                 |
--|                               ,p_summ_det => 'SUMMARY');                   |
--+----------------------------------------------------------------------------+
PROCEDURE dept_report 
( p_department_id        IN     NUMBER   DEFAULT 0
, p_dname                IN     VARCHAR2 DEFAULT NULL
, p_summ_det             IN     VARCHAR2 DEFAULT 'DETAIL'
)
IS
lcc_subprogram        CONSTANT    VARCHAR2(30)    := 'dept_report';
lvc_dname                         dept.dname%type := null;
lvn_department_id                 dept.department_id := 0;
lvn_ctr                           NUMBER := 0;
lvn_tot_sal                       NUMBER := 0;
BEGIN
    BEGIN
        SELECT dept.department_id, dept.dname
        INTO lvn_department_id, lvc_dname
        FROM dept 
        WHERE dept.department_id = case when p_department_id != 0 then p_department_id else get_dept_id (p_dname => p_dname) end
    EXCEPTION
    WHEN OTHERS THEN 
        dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE()); 
        RETURN;
    END IF;
    
    IF lvc_dname is not null and lvn_department_id > 0 and p_summ_det = 'DETAIL' THEN 
        FOR i in ( SELECT rownum, emp.* FROM emp where emp.department_id = lvn_department_id ) LOOP
            IF i.rownum = 1 THEN 
                dbms_output.put_line ( '                     Report to display employees for department : ' || lvc_dname || '(' || to_char(lvn_department_id) || ')') ;
                dbms_output.put_line( rpad('-',150,'-') );
                dbms_output.put_line( ' ' );
                dbms_output.put_line ( rpad('id',10,' ') || ' ' || rpad('Name',50,' ') || ' ' || rpad('Job Title',50,' ') || ' ' || rpad('ManagerId',10,' ') || ' ' 
                                    || rpad('DateHired',10,' ') || ' ' || rpad('Salary',10,' '));
                dbms_output.put_line ( rpad('-',10,'-') || ' ' || rpad('-',50,'-') || ' ' || rpad('-',50,'-') || ' ' || rpad('-',10,'-') || ' ' 
                                    || rpad('-',10,'-') || ' ' || rpad('-',10,'-'));                    
            END IF;
            dbms_output.put_line ( rpad(to_char(i.employee_id),10,' ') || ' ' || rpad(i.ename,50,' ') || ' ' || rpad(i.job_title,50,' ') || ' ' || rpad(to_char(i.mgr_id),10,' ') || ' ' 
                            || rpad(to_char(i.hiredate,'DD/MM/YYYY'),10,' ') || ' ' || rpad(to_char(i.salary),10,' '));
            lvn_ctr := lvn_ctr + 1;
        END LOOP;
        IF lvn_ctr  = 0 THEN 
            dbms_output.put_line('No employee record found for the department : ' || lvc_dname);
        ELSIF
            dbms_output.put_line(rpad('-',150,'-'));
            dbms_output.put_line('Total ' || to_char(lvn_ctr) || ' employees found for deparment : ' || lvc_dname || '(' || to_char(lvn_department_id) || ')');
        END IF;
    ELSIF lvc_dname is not null and lvn_department_id > 0 and p_summ_det = 'SUMMARY' THEN
        BEGIN
        
            SELECT sum(emp.salary) tot_sal, count(*) tot_emp
            INTO lvn_tot_sal, lvn_ctr
            FROM emp 
            WHERE emp.department_id = lvn_department_id 
            GROUP BY emp.department_id;
            
            IF lvn_ctr > 0 THEN 
                dbms_output.put_line ( '                     Report to display total employee salary for department : ' || lvc_dname || '(' || to_char(lvn_department_id) || ')') ;
                dbms_output.put_line( rpad('-',150,'-') );
                dbms_output.put_line( ' ' );
                dbms_output.put_line ( 'Total Salary for the department : ' || to_char(lvn_tot_sal) ', with total : ' || to_char(lvn_ctr) || ' employees');
                dbms_output.put_line( ' ' );
                dbms_output.put_line( rpad('-',150,'-') );
            END IF;
        EXCEPTION
        WHEN OTHERS THEN 
            dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE());  
        END ;
    ELSIF
        dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' ||'Incorrect value derived for deparment id.');
    END IF;    
EXCEPTION
WHEN OTHERS THEN 
    dbms_output.put_line('EXCEPTION: ' gcc_pkg_name || '.' || lcc_subprogram || ' : ' || SQLERRM || ':' 
                       || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE());  
END dept_report;

END employee;
/
SHOW ERRORS
/

EXIT;
REM ************************************************
REM              End of Script
REM ************************************************
